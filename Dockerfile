FROM eclipse-temurin:21-jre
COPY target/guestbook-1.0-SNAPSHOT.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/app.jar"]