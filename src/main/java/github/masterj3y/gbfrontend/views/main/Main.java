package github.masterj3y.gbfrontend.views.main;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.charts.model.Label;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Menu;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import github.masterj3y.gbfrontend.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@PageTitle("My View")
@Menu(icon = "line-awesome/svg/pencil-ruler-solid.svg", order = 0)
@Route(value = "")
@RouteAlias(value = "")
public class Main extends Composite<VerticalLayout> {


    private RestTemplate restTemplate = new RestTemplate();

    private final String backendService;

    public Main(
            @Value("${backend_service}")
            String backendService
    ) {
        this.backendService = backendService;
        TextField textField = new TextField();
        Button buttonPrimary = new Button();
        getContent().setWidth("100%");
        getContent().getStyle().set("flex-grow", "1");
        textField.setLabel("Book Name");
        textField.setWidth("min-content");
        buttonPrimary.setText("Add");
        buttonPrimary.setWidth("min-content");
        buttonPrimary.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getContent().add(textField);
        getContent().add(buttonPrimary);

        List<String> books = getBooks().stream().map(book -> book.getTitle()).toList();

        ListBox<String> list = new ListBox<>();
        list.setItems(books);
        getContent().add(list);

        buttonPrimary.addClickListener(buttonClickEvent -> {
            try {
                sendPostRequest(textField.getValue());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void sendPostRequest(String title) throws Exception {
        String jsonBody= "{ \"title\": \"" + title + "\", \"pages\": 223}";
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + backendService + ":8080/"))
                .POST(HttpRequest.BodyPublishers.ofString(jsonBody, StandardCharsets.UTF_8))
                .header("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        // Process the response
    }


    public List<Book> getBooks() {
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<List<Book>> response = restTemplate.exchange(
                "http://" + backendService + ":8080/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

}
